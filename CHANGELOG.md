# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [6.0.0] - 2020-09-10
### Changed
- `package.json` - Updated.
- `CHANGELOG.md` - Updated.
- `assets/js/controller/tabs.js` - Fixed accessibility.
- `assets/scss/components/_tabs.scss` - Fixed styles to switch links to buttons.
- `assets/scss/base/_user-generated-content.scss` - Remove outline css issue.
- `assets/js/controller/toggle.js` - formatting.

## [5.0.5] - 2020-05-21
### Changed
- **ALL UTILITY SCSS** - Fixed upper limit on media query.

## [5.0.4] - 2020-05-21
### Changed
- **ALL UTILITY SCSS** - Renamed more responsive classes.

## [5.0.3] - 2020-05-21
### Changed
- `assets/scss/lima.scss` - readd accidental deletions.

## [5.0.2] - 2020-05-21
### Changed
- `package.json` - Updated.
- `CHANGELOG.md` - Updated.
- `assets/scss/base/_buttons.scss` - changed cursor state on disabled buttons.
- **ALL SCSS** - formatting.
- **ALL UTILITY SCSS** - Make more responsive.

### Added
- `assets/scss/vendor/sass-mediaqueries/_mqpacker-ordering.scss` - added to help media queries build in the correct order.

## [5.0.1] - 2020-02-28
### Changed
- `package.json` - Updated.
- `CHANGELOG.md` - Updated.
- `assets/js/controller/smoothscroll.js` - updated scrolling class, based on the element that actually scrolls, rather than the body.
- `assets/scss/base/_wrapper.scss` - added **scroll-behavior: smooth;** style to **.b-wrapper**.
- `assets/scss/abstracts/_vars.scss` - added utility vars & updated the buttons map for addition of focus shadow.
- `assets/scss/base/_buttons.scss` - added **.b-button--transparent** for non-buttoney styled buttons, added focus shadow.

## [5.0.0] - 2020-02-11
### Removed
- `assets/js/controller/selectTruncation.js` - Updated the markup for selects that removes the need for a js solution.
  
### Changed
- `assets/js/lima.js` - Removed intialisation code for the removed script.
- `assets/scss/abstracts/_vars.scss` - Removed display variables as the element has been removed from the markup.
- `assets/scss/base/form-elements/_select.scss` - Amended the styling to reflect non js solution for select inputs.

## [4.0.0] - 2020-02-11
### Changed
- `package.json` - Updated.
- `CHANGELOG.md` - Updated.
- `assets/js/controller/navAccessibility.js` - updated ABEM naming for the nav component.
- `assets/js/lima.js` - updated initialisation of nav code with **.js-nav** rather than **.b-nav**.
- `assets/scss/lima.scss` - Changed due to the additions and removals below.
- `assets/scss/abstracts/_vars.scss` - Added vars for selects and checkbox/radio labels.
- `assets/scss/base/_global.scss` - Changed to help stop multi vertical scrollbars.
- `assets/scss/base/_section.scss` - Changed to help stop multi vertical scrollbars.
- `assets/scss/base/form-elements/_checkbox-radio.scss` - Added vars for more control of styling.
- `assets/scss/base/form-elements/_select.scss` - Added vars for more control of styling.

### Removed
- `assets/scss/base/_navs.scss` - Moved and renamed due to ABEM naming error.
- `assets/scss/base/navs/_alignment.scss` - Moved and renamed due to ABEM naming error.
- `assets/scss/base/navs/_breadcrumb.scss` - Moved and renamed due to ABEM naming error.
- `assets/scss/base/navs/_pagination.scss` - Moved and renamed due to ABEM naming error.

### Added
- `assets/scss/components/_navs.scss` - Moved and renamed due to ABEM naming error.
- `assets/scss/components/navs/_alignment.scss` - Moved and renamed due to ABEM naming error.
- `assets/scss/components/navs/_breadcrumb.scss` - Moved and renamed due to ABEM naming error.
- `assets/scss/components/navs/_pagination.scss` - Moved and renamed due to ABEM naming error.
- `assets/scss/base/_wrapper.scss` - Moved to its own file.

## [3.0.2] - 2020-01-31
### Changed
- `package.json` - update.
- `CHANGELOG.md` - update.
- `assets/scss/lima.scss` - fixed form-elements in main file.

## [3.0.1] - 2020-01-31
### Changed
- `package.json` - update.
- `CHANGELOG.md` - update.
- `assets/scss/base/_typography.scss` - fixed line-height issue.

## [3.0.0] - 2020-01-31
### Changed
- `README.md` - update.
- `package.json` - update.
- `CHANGELOG.md` - update.
- `assets/scss/lima.scss` - moved form-elements into main scss file.
- `assets/scss/abstracts/_vars.scss` - various.
- `assets/scss/base/_lazyload.scss` - fix ABEM naming.
- `assets/scss/base/_buttons.scss` - fix disabled styles.
- `assets/scss/base/_forms.scss` - moved form-elements references to **lima.scss**.
- `assets/scss/base/form-elements/_label.scss` - added colour var in variables.
- `assets/scss/base/form-elements/_legend.scss` - added colour var in variables.
- `assets/scss/base/form-elements/_global.scss` - added font-sze and line-height vars.
- `assets/scss/base/form-elements/_input-textarea.scss` - added font-sze and line-height vars.
- `assets/scss/vendor/flexboxgrid-sass/_flexboxgrid.scss` - fix minus values.
- `assets/js/lima.js` - Remove placeholder fallback js reference.
- `assets/js/controller/lazyload.js` - fix ABEM naming.

### Removed
- `assets/js/controller/placeholder.js` - remove placeholder fallback.

## [2.0.8] - 2020-01-10
### Changed
- `README.md` Link to the docs.

## [2.0.7] - 2019-11-03
### Fixed
- half margin and padding utility classes.

## [2.0.6] - 2019-11-03
### Changed
- Add responsive capability to border, margin, padding &amp; typography utility classes.

## [2.0.5] - 2019-11-03
### Fixed
- Date in changelog

## [2.0.4] - 2019-11-03
### Changed
- `/assets/scss/utilities/_display.scss` changed the display classes so they had the word display in them for semantic purposes.

## [2.0.1 & 2.0.2 & & 2.0.3] - 2019-11-02
### Fixed
- `assets/scss/base/form-elements/_states.scss` fixed the checkbox/radio references.

## [2.0.0] - 2019-11-02
### Fixed
- All styles for forms and inputs to make them better follow **ABEM** class naming convention.
- Split apart the `/assets/scss/utilities/_utilities.scss` file into separate more concise utility files `/assets/scss/utilities/*.scss`.

### Added
- media query to remove all animations and transitions for people that prefer not to see them `/assets/scss/base/_reboot.scss`.
- More utility classes.
  
### Changed
- Some of the existing utility classes to allow for responsive utilities.

## [1.1.1] - 2019-03-25
### Fixed
- **SCSS** remove function from `$base-line-height` definition.

## [1.1.0] - 2019-03-23
### Added
- Simple lightbox

### Changed
- Updated the changelog with all previous versions.
- Removed jquery require in lazyload js (it was unneeded).


## [1.0.1] - 2019-03-22
### Changed
- Remove link from readme.


## [1.0.0] - 2019-03-22
### Added
- **MISC** Upped version number to 1.0.0 to correctly start [semantic versioning](https://semver.org/)
- **MISC** Added a changelog.md

### Removed
- **MISC** Removed changelog from readme.md
- **SCSS** Remove hover and focus states on *.b-hide* as it was breaking the file upload form input

### Changed
- **SCSS** & **JS** Amend code for accordions to allow for nesting


## [0.1.28] - 2019-03-16
### Fixed
- **JS** - fix smoothscroll


## [0.1.27] - 2019-03-16
### Fixed
- **JS** - fix smoothscroll


## [0.1.26] - 2019-03-16
### Accidentally skipped


## [0.1.25] - 2019-03-16
- **JS** - Fix smoothscroll for linking to named targets
- **JS** - Fix slider lazyload functions for images with no lazyload
- **SCSS** - ordering of main scss so vars is at the top
- **JS** - convert tabs, select, tooltips, range input, file input, modal links, datepicker inputs to initialise with js-* classname (for clarity of elements with js)
- **SCSS** - Fix modal (old browsers), so it has no height or width when `.c-modal-open` is not present on the body.


## [0.1.24] - 2019-03-16
- **SCSS** - Remove hover states on checkboxes and radio buttons.
- **SCSS** - Add *.b-link* & *.b-link--block* classes to typography.
- **SCSS** - Amend *.b-image--block* style to add display block.
- **SCSS** - Fix vars $headings open sans declaration.
- **JS** - Add toggle component, which will allow for easy creation of element toggling.
- **SCSS** & **JS** - Add lazyload
- **SCSS** - Add *get-line-height* & *aspect-ratio* scss functions
- **SCSS** - Add *.u-ellipsis* style for automatic truncation of text with an ellipsis (need to add width in specific style for elements this is added to).
- **SCSS** - Amend buttons styles control by the variables stylesheet.
- **JS** - Add more data attributes for slider component, make it initialise with js-slider class, make variable declarations turnery statements.
- **JS** - Add custom lazyload function to slider js to accommodate for our own lazyload component
- **JS** - make accordion and tab components intialise with js-* classnames


## [0.1.23] - 2019-02-20
### Changed
- **SCSS** convert heading font sizes to use `px-to-rem()` function.


## [0.1.22] - 2019-02-20
### Fixed
- **SCSS** add `-ms-interpolation-mode: bicubic;` to `.b-image` as a fix for image resizing in IE.
- **SCSS** add fix to flexboxgrid-sass vendor override for IE.


## [0.1.21] - 2019-02-20
### Accidently skipped


## [0.1.20] - 2018-10-02
### Fixed
- **JS** fix require in datepicker controller to access the correct file for date pickers.


## [0.1.19] - 2018-09-11
### Changed
- **SCSS** Added border radius to form inputs.


## [0.1.18] - 2018-08-17
### Fixed
- **SCSS** Fix form states.


## [0.1.17] - 2018-08-17
### Fixed
- **SCSS** Form state amends.


## [0.1.16] - 2018-08-17
### Fixed
- **SCSS** Fix range input.


## [0.1.15] - 2018-08-17
### Fixed
- **SCSS** Fix form-group margins.


## [0.1.14] - 2018-08-17
### Fixed
- **SCSS** make sure select and file uploads styles are completely controlled by variables.


## [0.1.13] - 2018-08-17
### Fixed
- **SCSS** variables for checkbox and radio buttons.
- **JS** task running issues.


## [0.1.12] - 2018-08-16
### Added
- **SCSS** Added variable control of form inputs
- **JS** Smoothscroll


## [0.1.11] - 2018-07-19
### Changed
- Version number


## [0.1.10] - 2018-07-19
### Fixed 
- **SCSS** slider button disabled issue.

### Changed
- Update *@chenfengyuan/datepicker* & *@vivid-web/flexboxgrid-sass* dependacies as the repositories had moved.

### Added
- **JS** & **SCSS** File upload component


## [0.1.9] - 2018-06-05
### Removed 
- **JS** Remove fastclick as it breaks in laravel builds, up version number, fire change event on load of select truncation to immediately fill the display

### Changed
- **JS** add `.change()` to the end of the select truncation js to triger a change on load.


## [0.1.8] - 2018-05-31
### Fixed
- **SCSS** var references to slick files.

### Added
- Add `.b-form__label` class an styles for labels.

### Changed
- **SCSS** Changed the px height and widths of checkboxes and radios to use `px-to-rem` function.
- **SCSS** Add `&.b-hide` style to `b-form__label` to remove margin.


## [0.1.7] - 2018-05-31
## [0.1.6] - 2018-05-31
## [0.1.5] - 2018-05-31
### Accidentally skipped


## [0.1.4] - 2018-05-31
### Fixed
- Scss var references to slick files.

### Removed
- offline stylesheet.
- empty mixin stylesheet.


## [0.1.3] - 2018-05-31
### Accidentally skipped


## [0.1.2] - 2018-05-31
### Remove
- Remove images from repo.
- Remove local versions of vendor files (only keep overrides).

### Add
- utilities scss

### Misc
- Move files to more appropriate places.


## [0.1.1] - 2018-04-06
### Changed
- Removed private flag from package.json.


## [0.1.0] - 2018-04-06
### Added
- First commit, adding basis files