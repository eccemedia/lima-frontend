var $ = (jQuery = require('jquery'));
require('./vendor/modernizr');

// IE Detection
require('./controller/ieDetect')();

// Polyfill for responsive images
var picturefill = require('picturefill');
window.picturefill();

// Freeze css animations on resize
require('./controller/resizeFreeze')();

// Navigation Accessibility
var navAccessibility = $('.js-nav');
if (navAccessibility.length > 0) {
    require('./controller/navAccessibility')();
}

// Tooltips
var tooltip = $('.js-tooltip');
if (tooltip.length > 0) {
    require('./controller/tooltips')(tooltip);
}

// Range inputs
var rangeInput = $('.js-range-input');
if (rangeInput.length > 0) {
    require('./controller/rangeInput')(rangeInput);
}

// date picker
var datePicker = $('.js-datepicker');
if (datePicker.length > 0) {
    require('./controller/datePicker')(datePicker);
}

// slider initialisations
var sliderInit = $('.js-slider');
if (sliderInit.length > 0) {
    require('./controller/sliderInit')(sliderInit);
}

// Accordion init
var accordion = $('.js-accordion');
if (accordion.length > 0) {
    require('./controller/accordions')();
}

// Tabs init
var tabs = $('.js-tabs');
if (tabs.length > 0) {
    require('./controller/tabs')();
}

// Modals init
var modals = $('.js-modal-link');
if (modals.length > 0) {
    require('./controller/modals')(modals);
}

// Smoothscroll init
var smoothscroll = $('.js-smoothscroll');
if (smoothscroll.length > 0) {
    require('./controller/smoothscroll')(smoothscroll);
}

// Fileupload
var fileUpload = $('.js-file-upload');
if (fileUpload.length > 0) {
    require('./controller/fileUpload')(fileUpload);
}

// toggle init
var toggle = $('.js-toggle');
if (toggle.length > 0) {
    require('./controller/toggle')(toggle);
}

// lazyload init
var lazyload = $('.js-lazyload');
if (lazyload.length > 0) {
    require('./controller/lazyload')(lazyload);
}

// lightbox init
var lightbox = $('.js-lightbox');
if (lightbox.length > 0) {
    require('./controller/lightbox')(lightbox);
}

// ServiceWorker is a progressive technology. Ignore unsupported browsers
// Uncomment console.logs when testing the service worker
// if ('serviceWorker' in navigator) {
//     console.log('CLIENT: service worker registration in progress.');
//     navigator.serviceWorker.register('/serviceWorker.js').then(function (registration) {
//         console.log('ServiceWorker registration successful with scope: ', registration.scope);
//     }, function (err) {
//         console.log('ServiceWorker registration failed: ', err);
//     });
// }
