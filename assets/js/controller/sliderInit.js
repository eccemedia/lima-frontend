var $ = jQuery = require('jquery');
require('slick-carousel/slick/slick');

module.exports = function(el) {
	el.each(function() {
		var autoplay = (typeof $(this).data('autoplay') !== 'undefined') ? $(this).data('autoplay') : false;

		var autoplaySpeed = (typeof $(this).data('autoplay-speed') !== 'undefined') ? $(this).data('autoplay-speed') : 6000;

		var fade = (typeof $(this).data('fade') !== 'undefined') ? $(this).data('fade') : true;

		var dots = (typeof $(this).data('dots') !== 'undefined') ? $(this).data('dots') : true;

		var dots = (typeof $(this).data('dots') !== 'undefined') ? $(this).data('dots') : true;

		var arrows = (typeof $(this).data('arrows') !== 'undefined') ? $(this).data('arrows') : false;

		var infinite = (typeof $(this).data('infinite') !== 'undefined') ? $(this).data('infinite') : false;

		var pauseOnHover = (typeof $(this).data('pause-on-hover') !== 'undefined') ? $(this).data('pause-on-hover') : false;

		var speed = (typeof $(this).data('speed') !== 'undefined') ? $(this).data('speed') : 300;

		var asNavFor = (typeof $(this).data('as-nav-for') !== 'undefined') ? $(this).data('as-nav-for') : '';

		var slidesToShow = (typeof $(this).data('slides-to-show') !== 'undefined') ? $(this).data('slides-to-show') : 1;

		var slidesToScroll = (typeof $(this).data('slides-to-scroll') !== 'undefined') ? $(this).data('slides-to-scroll') : 1;

		var focusOnSelect = (typeof $(this).data('focus-on-select') !== 'undefined') ? $(this).data('focus-on-select') : false;

		var responsive = (typeof $(this).data('responsive') !== 'undefined') ? $(this).data('responsive') : [];
		
		$(this).slick({
			prevArrow: '<a class="slick-btn slick-prev"></a>',
			nextArrow: '<a class="slick-btn slick-next"></a>',
			accessibility: true,
			rows: 0,
			mobileFirst: true,
			autoplay: autoplay,
			autoplaySpeed: autoplaySpeed,
			fade: fade,
			dots: dots,
			arrows: arrows,
			infinite: infinite,
			pauseOnHover: pauseOnHover,
			speed: speed,
			asNavFor: asNavFor,
			slidesToShow: slidesToShow,
			slidesToScroll: slidesToScroll,
			focusOnSelect: focusOnSelect,
			responsive: responsive
		}).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
			var imageToLoad = $(slick.$slides[nextSlide]).find('.js-lazyload');

			if (typeof imageToLoad[0] !== 'undefined') {
				lazyLoadImage(imageToLoad);
			}
		}).on('afterChange', function (event, slick, currentSlide) {
			var imageToLoad = $(slick.$slides[currentSlide]).next().find('.js-lazyload');

			if (typeof imageToLoad[0] !== 'undefined') {
				lazyLoadImage(imageToLoad);
			}
		});

		function lazyLoadImage(imageToLoad) {
			if (imageToLoad[0].hasAttribute('data-src')) {
				imageToLoad.attr({ src: imageToLoad.attr('data-src') }).removeAttr('data-src');
			}

			if (imageToLoad[0].hasAttribute('data-srcset')) {
				imageToLoad.attr({ srcset: imageToLoad.attr('data-srcset') }).removeAttr('data-srcset');
			}
		}
	});
}