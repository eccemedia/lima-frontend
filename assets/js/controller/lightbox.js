var $ = (jQuery = require('jquery')),
    SimpleLightbox = require('simple-lightbox');

module.exports = function (el) {
    // for each gallery grouping
    el.each(function () {
        $(this).find('.js-lightbox-link:not(".js-lightbox-link--video"):not(".js-lightbox-link--iframe")').simpleLightbox({
            htmlClass: ' c-lightbox-open',
            elementClass: ' c-lightbox__element',
            elementLoadingClass: ' c-lightbox__loading',
            closeBtnClass: ' c-lightbox__close-button',
            nextBtnClass: ' c-lightbox__next-button',
            prevBtnClass: ' c-lightbox__prev-button',
            overlayClass: ' c-lightbox__overlay'
        });
    });

    // Video or iframe links
    // these are not included in the links above as they
    // will have to be separate from normal "galleries"/"grouping"
    $('.js-lightbox-link--video, .js-lightbox-link--iframe').each(function () {
        var $this = $(this),
            clickObject = {
                content: null,
                htmlClass: ' c-lightbox-open',
                elementClass: ' c-lightbox__element',
                elementLoadingClass: ' c-lightbox__loading',
                closeBtnClass: ' c-lightbox__close-button',
                nextBtnClass: ' c-lightbox__next-button',
                prevBtnClass: ' c-lightbox__prev-button',
                overlayClass: ' c-lightbox__overlay'
            };

        if ($this.hasClass('js-lightbox-link--video')) {
            var mp4 = $this.data('mp4') !== 'undefined' ? '<source src="' + $this.data('mp4') + '" type="video/mp4">' : null;
            var webm = $this.data('webm') !== 'undefined' ? '<source src="' + $this.data('webm') + '" type="video/mp4">' : null;
            var ogg = $this.data('ogg') !== 'undefined' ? '<source src="' + $this.data('ogg') + '" type="video/mp4">' : null;

            clickObject.content = '<video id="js-banner-video" class="c-lightbox__video" autoplay="" loop="" width="100%" controls>' + mp4 + webm + ogg + '</video>';

        } else if ($this.hasClass('js-lightbox-link--iframe')) {
            clickObject.content = $this.attr('href');
        }

        $this.on('click', function (e) {
            e.preventDefault();

            SimpleLightbox.open(clickObject);
        });
    });
};