var $ = jQuery = require('jquery');

module.exports = function(el) {

	// open modal from clicking button with modal attributes
	el.on('click', function (e) {
		e.preventDefault();

		var modalLinkTarget = $(this).attr('data-target');

		if (modalLinkTarget != '' && $(modalLinkTarget)[0]) {

			$('body').addClass('c-modal-open');

			setTimeout(function () {
				$(modalLinkTarget).addClass('c-modal--show').attr({'aria-hidden': 'false'}).removeAttr('tabindex');
			}, 500);

			if($(modalLinkTarget).hasClass('c-modal--fade')) {
				
				setTimeout(function () {
					$(modalLinkTarget).find('.c-modal__content').addClass('c-modal__content--show');
				}, 1000);
				
			} else {
				
				setTimeout(function () {
					$(modalLinkTarget).find('.c-modal__content').addClass('c-modal__content--show');
				}, 500);
				
			}

		} else {
			alert('Modal or modal link do not exist');
		}
		
	});

	// close an open modal
	$('.c-modal__close').on('click', function (e) {
		e.preventDefault();

		var closeLinkParentModal = $(this).parents('.c-modal'),
			closeLinkSiblingContent = closeLinkParentModal.find('.c-modal__content');

		closeLinkSiblingContent.removeClass('c-modal__content--show');

		if (closeLinkParentModal.hasClass('c-modal--fade')) {

			setTimeout(function () {
				closeLinkParentModal.removeClass('c-modal--show').attr({'aria-hidden': 'true'}).attr({'tabindex': '-1'});
			}, 500);

			setTimeout(function () {
				$('body').removeClass('c-modal-open');
			}, 1000);

		} else {

			closeLinkParentModal.removeClass('c-modal--show').attr({ 'aria-hidden': 'true' }).attr({ 'tabindex': '-1' });

			setTimeout(function () {
				$('body').removeClass('c-modal-open');
			}, 500);

		}
	});

}