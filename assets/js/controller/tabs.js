var $ = (jQuery = require("jquery"));

module.exports = function (el) {
  // when tab link is clicked
  $(".c-tabs__link").on("click", function (e) {
    e.preventDefault();

    if (!$(this).hasClass("c-tabs__link--active")) {
      // vars
      var thisTabGroup = $(this).parents(".c-tabs"); // this links parent tab

      // Remove other active classes from other links and content panes
      thisTabGroup
        .find(".c-tabs__link--active")
        .removeClass("c-tabs__link--active")
				.attr({
					"aria-selected": "false",
					"tabindex": "-1"
				});
      thisTabGroup
        .find(".c-tabs__tab--active")
        .removeClass("c-tabs__tab--active")
        .attr({"hidden": ""});

      // Add active class to this link
      $(this)
        .addClass("c-tabs__link--active")
        .attr({ 
					"aria-selected": "true", 
					"tabindex": "0" 
				});

      // Get this links data-target, which has the target panes id value
      var currentLinkTarget = $(this).attr("data-target");

      // Make current links content pane visible by adding active class
      thisTabGroup
        .find(currentLinkTarget)
        .addClass("c-tabs__tab--active")
				.removeAttr("hidden");
    }
  });
};
