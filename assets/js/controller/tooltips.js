var $ = jQuery = require('jquery');
require('../vendor/modernizr');

module.exports = function(el) {

	// toggle the open class for the tooltip
	function tooltipOpener($this) {
		$this.addClass('b-tooltip--open');
		$this.find('.b-tooltip__message').attr({'aria-hidden': 'false','tabindex':'0'});
	}

	// toggle the open class for the tooltip
	function tooltipCloser($this) {
		$this.removeClass('b-tooltip--open');
		$this.find('.b-tooltip__message').attr({'aria-hidden': 'true','tabindex':'-1'});
	}
	
	// Using modernizr, check if the browser/device is touch screen or not
	// if it is a touch screen
	if (Modernizr.touchevents) {
		// clicks
		el.click(function (event) {
			event.stopPropagation();

			if(!$(this).hasClass('b-tooltip--open')) {
				tooltipOpener($(this));
			} else {
				tooltipCloser($(this));
			}
		});

	// else it is not a touch screen
	} else {
		// Hovers
		// If this is a tooltip and not a popover, then open through hovers
		el.hover(function (event) {
			// If current tooltip doesn't have class of popover
			if (!$(this).hasClass('b-tooltip--popover')) {
				tooltipOpener($(this));
			}
		}, function () {
			// If current tooltip doesn't have class of popover
			if (!$(this).hasClass('b-tooltip--popover')) {
				tooltipCloser($(this));
			}
		});

		// Clicks
		// if this tooltip is a popover, then open through clicks
		el.click(function (event) {
			event.stopPropagation();

			if ($(this).hasClass('b-tooltip--popover')) {
				if (!$(this).hasClass('b-tooltip--open')) {
					tooltipOpener($(this));
				} else {
					tooltipCloser($(this));
				}
			}
		});
	}

	// if a tooltip/popover is open, and you click anywhere else then close the tooltip/popover.
	$(window).click(function (event) {
		if ($('.b-tooltip--open')[0]) {
			$('.b-tooltip--open').removeClass('b-tooltip--open');
		}
	});

}