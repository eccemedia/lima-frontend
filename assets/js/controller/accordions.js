var $ = jQuery = require('jquery');

module.exports = function(el) {

	// Adding aria labels for accessibility
	$('.c-accordion__title').attr({'aria-selected':'false'});
	$('.c-accordion__content').attr({'aria-hidden':'true'});
	
	// on click of accordion title
	$('.c-accordion__title').on("click", function () {
		// switch out aria labels based on whether the accordion is open or not
		if(!$(this).parent('.c-accordion').hasClass('c-accordion--open')) {
			$(this).attr({'aria-selected':'true'}).attr({'tabindex': '0'});;
			$(this).next('.c-accordion__content').attr({'aria-hidden': 'false'});
			$(this).next('.c-accordion__content').find('.b-ugc').attr({'tabindex': '-1'});
		} else {
			$(this).attr({'aria-selected':'false'}).removeAttr('tabindex');;
			$(this).next('.c-accordion__content').attr({'aria-hidden': 'true'});
			$(this).next('.c-accordion__content').find('.b-ugc').removeAttr('tabindex');
		}

		// add open class and slide toggle the content portion
		$(this).parent('.c-accordion').toggleClass('c-accordion--open');
		$(this).next('.c-accordion__content').slideToggle(250);
	});

}