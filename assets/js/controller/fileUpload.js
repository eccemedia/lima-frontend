var $ = jQuery = require('jquery');

module.exports = function (el) {

	// On click of span mock file, trigger click of real hidden file upload
	el.on("click", function () {
		$(this).next('input[type="file"]').click();
	});

	//on change of input, type file
	$('.b-form__file-upload-container input[type="file"]').on('change', function (e) {
		var thisElement = $(this)
			parentContainer = thisElement.parents('.b-form__file-upload-container'),
			siblingElement = parentContainer.find('.b-form__file-upload'),
			siblingContent = siblingElement.html(),
			fileName = '';

		// if the input has more than one file uplaoded
		if (thisElement[0].files[0] && thisElement[0].files.length > 1) {
			fileName = thisElement.data('multiple-caption').replace('{count}', thisElement[0].files.length);
		
		// else if there is one file uploaded
		} else if (e.target.value) {
			fileName = e.target.value.split('\\').pop();
		}

		// if fileName exists
		if (fileName) {
			// make the input display the fileName var
			siblingElement.html(fileName);
		} else {
			// make the input display the original"upload a file" message
			siblingElement.html(siblingContent);
		}
	});

}