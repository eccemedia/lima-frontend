var $ = jQuery = require('jquery');

module.exports = function(){
	// Detect if is IE
	function msieversion() {

	    var ua = window.navigator.userAgent,
	        emptyVar;

	    // IE 10 or older
	    var msie = ua.indexOf('MSIE ');
	    if (msie > 0) {

	        emptyVar = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
	        $('html').addClass('ie'+emptyVar);
	        return;
	    }

	    // IE 11 => return version number
	    var trident = ua.indexOf('Trident/');
	    if (trident > 0) {

	        var rv = ua.indexOf('rv:');
	        emptyVar = parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
	        $('html').addClass('ie'+emptyVar);
	        return;
	    }

		// Edge (IE 12+) => return version number
		var edge = ua.indexOf('Edge/');
		if (edge > 0) {
			$('html').addClass('ieEdge');
	        return;
		}

	    // other browser
	    return false;
	}

	msieversion();
}