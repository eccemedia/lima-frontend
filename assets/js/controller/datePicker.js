var $ = (jQuery = require('jquery'));
require('@chenfengyuan/datepicker/dist/datepicker.js');

module.exports = function(el) {
	el.each(function() {
		var start = $(this).data('start'),
			end = $(this).data('end');

		$(this).datepicker({
			format: 'dd/mm/yyyy',
			startDate: start,
			endDate: end,
			autoHide: true
		});
	});
};
