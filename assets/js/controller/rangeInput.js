var $ = jQuery = require('jquery');

module.exports = function(el) {
	
	// put range inputs content into the output tag
	// so you can visually see the value of the range
	$(el).on('input change', function () {
		$(this).next('.b-form__range-output').html($(this).val());
	});

	// Reset bug for range input not resetting to zero
	// fire change on reset
	$(el).parents('form').on('reset', function () {
		$(el).change();
		$(el).next('.b-form__range-output').html(0);
	});

}