var $ = jQuery = require('jquery');

module.exports = function () {

	// Drop downs built for accesibility, idea and execution from:
	// https://www.w3.org/WAI/tutorials/menus/flyout/
	// https://www.w3.org/WAI/tutorials/menus/application-menus/
	// https://a11y.nicolas-hoffmann.net/subnav-dropdown/

	$(document).ready(function () {

		// events on main menu
		// 1. Mouse events (hovering)
		// CORRECT
		$('body').on('mouseenter', '.c-nav__list-element', function (event) {

			var $this = $(this);

			if ($this.hasClass('c-nav__list-element--has-children')) {
				$this.addClass('c-nav__list-element--has-children-open');
				$this.attr({ 'aria-expanded': 'true' });
			}

		}).on('mouseleave', '.c-nav__list-element', function (event) {

			var $this = $(this);

			// leave delay, described below
			// https://www.w3.org/WAI/tutorials/menus/flyout/
			setTimeout(function () {
				if ($this.hasClass('c-nav__list-element--has-children')) {
					$this.removeClass('c-nav__list-element--has-children-open');
					$this.attr({ 'aria-expanded': 'false' });
				}
			}, 500);

		});

		// 2. Keyboard events (for accessibility)
		$('body').on('focus', '.c-nav__link', function (event) {

			var $this = $(this),
				$parent_item = $this.parent('.c-nav__list-element');

			if ($parent_item.hasClass('c-nav__list-element--has-children')) {
				$parent_item.addClass('c-nav__list-element--has-children-open');
				$parent_item.attr({ 'aria-expanded': 'true' });
			}

		}).on('focusout', '.c-nav__link', function (event) {

			// ----------------------------
			// CUREENTLY HIGHLIGHT OUT
			// This is due to the code
			// hiding the subnavs, when
			// trying to enter them
			// ----------------------------

			// var $this = $(this),
			// 	$parent = $this.parents('.c-nav__list'),
			// 	$parent_item = $this.parent('.c-nav__list-element');

			// if ($parent_item.hasClass('c-nav__list-element--has-children')) {
			// 	$parent_item.removeClass('c-nav__list-element--has-children-open');
			// 	$parent_item.attr({'aria-expanded': 'false'});
			// }

		}).on('keydown', '.c-nav__link', function (event) {

			var $this = $(this),
				$parent = $this.parents('.c-nav__list'),
				$parent_item = $this.parent('.c-nav__list-element'),
				$subnav = $this.next('.c-nav__list--child');

			// Event on keyboard left button
			// CORRECT
			if (event.keyCode == 37) {
				// select previous nav-system__link

				// if we are on first => activate last
				if ($parent_item.is('.c-nav__list-element:first-child')) {
					$parent.children('.c-nav__list-element:last-child').children('.c-nav__link').focus();

				// else activate previous
				} else {
					$parent_item.prev().children('.c-nav__link').focus();
				}

				if ($parent_item.hasClass('c-nav__list-element--has-children')) {
					$parent_item.removeClass('c-nav__list-element--has-children-open');
					$parent_item.attr({'aria-expanded': 'false'});
				}

				event.preventDefault();
			}

			// Event on keyboard right button
			// CORRECT
			if (event.keyCode == 39) {

				// if we are on last => activate first
				if ($parent_item.is('.c-nav__list-element:last-child')) {
					$parent.children('.c-nav__list-element:first-child').children('.c-nav__link').focus();

				// else activate next
				} else {
					$parent_item.next().children('.c-nav__link').focus();
				}

				if ($parent_item.hasClass('c-nav__list-element--has-children')) {
					$parent_item.removeClass('c-nav__list-element--has-children-open');
					$parent_item.attr({'aria-expanded': 'false'});
				}

				event.preventDefault();

			}

			// Event on keyboard pressing down button or space
			// CORRECT
			if (event.keyCode == 40 || event.keyCode == 32) {
				// If there is a subnav on current item
				if ($subnav.length === 1) {
					// SElect first child list item in subnav and focus
					$subnav.find('.c-nav__list-element--child:first-child').children('.c-nav__link--child').focus();
				}
				event.preventDefault();
			}

			// Event on keyboard pressing shift & tab buttons
			// CORRECT
			if (event.shiftKey && event.keyCode == 9) {
				// if the current main menu list item is not the first menu item
				if (!$parent_item.is('.c-nav__list-element:first-child')) {

					// vars
					// previous main menu list item
					var $prev_nav_element = $parent_item.prev('.c-nav__list-element');
					// subnav of previous main menu list item
					var $subnav_prev = $prev_nav_element.find('.c-nav__list--child');

					// If the previous main list element has a subnav
					if ($subnav_prev.length === 1) {
						
						// Make previous link element open
						$prev_nav_element.addClass('c-nav__list-element--has-children-open');
						$prev_nav_element.attr({'aria-expanded': 'true'});

						// if current menu item has subnva hide current subnav
						if ($parent_item.hasClass('c-nav__list-element--has-children')) {
							$parent_item.removeClass('c-nav__list-element--has-children-open');
							$parent_item.attr({'aria-expanded': 'false'});
						}

						// focus last element in elements subnav
						$subnav_prev.find('.c-nav__list-element--child:last-child').children('.c-nav__link--child').focus();
					
					// else the previous main list element has no subnav
					} else {

						// Focus on previous main link
						$prev_nav_element.find('.c-nav__link').focus();

					}
					
					event.preventDefault();

				}
			}

		});

		// Keyboard events in submenu items
		$('body').on('keydown', '.c-nav__link--child', function (event) {

			var $this = $(this),
				$subnav = $this.parents('.c-nav__list--child'),
				$subnav_item = $this.parent('.c-nav__list-element--child'),
				$nav_link = $subnav.prev('.c-nav__link'),
				$nav_item = $nav_link.parents('.c-nav__list-element'),
				$nav = $nav_link.parents('.c-nav__list');

			// Event on keyboard pressing down button
			// CORRECT
			if (event.keyCode == 40) {
				// if we are on last => activate first
				if ($subnav_item.is('.c-nav__list-element--child:last-child')) {
					$subnav.find('.c-nav__list-element--child:first-child ').children('.c-nav__link--child').focus();

					// else activate next
				} else {
					$subnav_item.next().children('.c-nav__link--child').focus();
				}
				event.preventDefault();
			}

			// Event on keyboard pressing up button
			// CORRECT
			if (event.keyCode == 38) {
				// if we are on first => activate last
				if ($subnav_item.is('.c-nav__list-element--child:first-child')) {
					$subnav.find(' .c-nav__list-element--child:last-child ').children('.c-nav__link--child').focus();

					// else activate previous
				} else {
					$subnav_item.prev().children('.c-nav__link--child').focus();
				}
				event.preventDefault();
			}

			// Event on keyboard pressing Esc button
			// CORRECT
			if (event.keyCode == 27) {
				$nav_link.focus();
				event.preventDefault();
			}

			// Event on keyboard pressing right button
			// CORRECT
			if (event.keyCode == 39) {
				// if we are on last => activate first and choose first item
				if ($nav_item.is('.c-nav__list-element:last-child')) {
					$next = $nav.find('.c-nav__list-element:first-child').children('.c-nav__link');
					$next.focus();
					$subnav_next = $next.next('.c-nav__list--child');

					if ($subnav_next.length === 1) {
						$subnav_next.find(' .c-nav__list-element--child:first-child ').children('.c-nav__link--child').focus();
					}

					// else activate next
				} else {
					// hide drop down you are getting away from
					$nav_item.removeClass('c-nav__list-element--has-children-open');
					$nav_item.attr({ 'aria-expanded': 'false' });

					$next = $nav_item.next().children('.c-nav__link');
					$next.focus();
					$subnav_next = $next.next('.c-nav__list--child');
					if ($subnav_next.length === 1) {
						$subnav_next.find(' .c-nav__list-element--child:first-child ').children('.c-nav__link--child').focus();
					}
				}
				event.preventDefault();
			}

			// Event on keyboard pressing left button
			// CORRECT
			if (event.keyCode == 37) {
				// if we are on first => activate last and choose first item
				if ($nav_item.is('.c-nav__list-element:first-child')) {
					$prev = $nav.find('.c-nav__list-element:last-child').children('.c-nav__link');
					$prev.focus();
					$subnav_prev = $prev.next('.c-nav__list--child');
					if ($subnav_prev.length === 1) {
						$subnav_prev.find('.c-nav__list-element--child:first-child ').children('.c-nav__link--child').focus();
					}

				// else activate prev
				} else {
					// hide drop down you are getting away from
					$nav_item.removeClass('c-nav__list-element--has-children-open');
					$nav_item.attr({'aria-expanded': 'false'});

					// focus on main link of previous main element
					$prev = $nav_item.prev().children('.c-nav__link');
					$prev.focus();

					// If that main element has a subnav then focus on the first
					// element in the subnav
					$subnav_prev = $prev.next('.c-nav__list--child');
					if ($subnav_prev.length === 1) {
						$subnav_prev.find('.c-nav__list-element--child:first-child').children('.c-nav__link--child').focus();
					
					// If no subnav 
					} else {
						// then hide all other subnavs
						$('.c-nav__list-element--has-children').removeClass('c-nav__list-element--has-children-open');
						$('.c-nav__list-element--has-children').attr({'aria-expanded': 'false'});
					}

				}
				event.preventDefault();
			}

			// Event on keyboard pressing the tab button
			// if we are on last subnav of last item and we go forward => hide subnav 
			// CORRECT
			if (event.keyCode == 9 && !event.shiftKey) {
				if ($subnav_item.is('.c-nav__list-element--child:last-child')) {
					$nav_item.removeClass('c-nav__list-element--has-children-open');
					$nav_item.attr({'aria-expanded': 'false'});
				}
			}

			// Event on keyboard pressing the space button
			// same as click item
			if (event.keyCode == 32) {
				$this[0].click();
			}

		});
	});

}