var $ = jQuery = require('jquery');

module.exports = function (el) {

	$(function () {
		setTimeout(function () {
			if (location.hash) {
				window.scrollTo(0, 0);
				target = location.hash.split('#');
				smoothScrollTo(target[1], $('#' + target[1]));
			}
		}, 1);

		el.click(function (e) {
			if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
				e.preventDefault();
				smoothScrollTo(this.hash.split('#')[1], $(this.hash));
				return false;
			}
		});

		function smoothScrollTo(target, targetID) {
			targetValue = targetID.length ? targetID : $('[name=' + target + ']');

			if (targetValue.length) {
				$('.b-wrapper').animate({
					scrollTop: targetValue.offset().top
				}, 1000);
			}
		}
	});

}