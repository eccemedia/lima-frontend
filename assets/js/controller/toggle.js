var $ = (jQuery = require("jquery"));

module.exports = function (el) {
  el.click(function (e) {
    e.preventDefault();

    // get target attribute of current link
    var target = $(this).attr("data-target");

    // check if the above has a value and is an element on the page
    if (typeof target !== "undefined" && target.length > 0 && $(target)[0]) {
      // toggle actual link
      $(this).toggleClass("is-open");

      // toggle nav next list
      $(target).toggleClass("is-open");
    }
  });
};
