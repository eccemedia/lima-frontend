var LazyLoad = require("vanilla-lazyload");

module.exports = function(el) {
  var myLazyLoad = new LazyLoad({
    elements_selector: ".js-lazyload",
    threshold: 300,
    class_loading: "b-lazyload__image--loading",
    class_loaded: "b-lazyload__image--loaded",
    class_error: "b-lazyload__image--error"
  });
};
