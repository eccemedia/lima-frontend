var $ = jQuery = require('jquery');

module.exports = function() {
	// resize event that will add a body class of freeze
	// till you stop resizing
	// this body class will stop css transitions on resize event
    var noAnimTimeout;
    $(window).resize(function(){
        $('body').addClass('freeze');
        clearTimeout(noAnimTimeout);
        noAnimTimeout = setTimeout(function(){
            $('body').removeClass('freeze');
        },150);
    });

    // On load remove freeze body class
    // the bodyclass is there as a preloader to stop
    // css transitions on load
	$('body').removeClass('freeze');
}